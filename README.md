# SonarQube Formula Plugin #

Formula is a SonarQube extension that shows values of new metrics created by the combination of others.

### Features ###

Would you like to know values of new metrics, result of other metrics combination, without implement it? Would be interesting to know the value of some operations applied to SonarQube metrics?

With SonarQube Formula Plugin is possible to define an expression or formula that combines some metrics (or numerical constants) and shows the result in a SonarQube widget.

Now you can create some manual metrics associated with a formula in order to be calculated and saved by Sonar.

## Download

[Version 1.3 - SonarQube 5.6.x (LTS)](https://www.qalitax.com/descargas/product/sonar-formula-plugin-1.3.jar?customerSurnames=customer&customerCompany=spanish&customerName=web_v2&customerEmail=customer@email.es)