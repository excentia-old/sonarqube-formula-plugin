/*
 * Sonar Formula Plugin
 * Copyright (C) 2012 excentia
 * contact@excentia.es
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02
 */
package es.excentia.sonar.plugins.formula;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;
import org.sonar.api.ce.measure.MeasureComputer;
import org.sonar.api.ce.measure.MeasureComputer.MeasureComputerDefinition.Builder;
import org.sonar.api.measures.CoreMetrics;
import org.sonar.api.measures.Measure;
import org.sonar.api.measures.MeasureUtils;
import org.sonar.api.measures.Metric;
import org.sonar.api.measures.MetricFinder;
import org.sonar.api.utils.log.Logger;
import org.sonar.api.utils.log.Loggers;

import es.excentia.sonar.plugins.formula.model.FormulaMetric;
import es.excentia.sonar.plugins.formula.util.FormulaUtil;

/**
 * Formula Plugin Decorator
 */
public class FormulaMeasureComputer implements MeasureComputer {

  private static final Logger LOG = Loggers.get(FormulaMeasureComputer.class);

  private static final String FORMULA_PREFIX = "Formula[";
  private static final String FORMULA_SUFFIX = "]";
  
  private final MetricFinder metricFinder;

  /**
   * Constructor
   * 
   * @param metricFinder the Metric finder
   */
  public FormulaMeasureComputer(MetricFinder metricFinder) {
    this.metricFinder = metricFinder;
  }

  @Override
  public MeasureComputerDefinition define(MeasureComputerDefinitionContext defContext) {

    LOG.debug("Building Formula definition...  ");

    Builder builder = defContext.newDefinitionBuilder();
    
    // we will depend upon all metrics
    builder = builder.setInputMetrics(getAllCoreMetricsKeys());
    
    // we will store three data metrics, with binary files for the reports
    builder = builder.setOutputMetrics(getFormulaMetrics());
    
    LOG.debug("Formula definition built.  ");

    return builder.build();
  }

  @Override
  public void compute(MeasureComputerContext context) {
    
    LOG.debug("Computing component to save formula measures...");
    
    // Manual metrics
    List<Metric> manualMetrics = getManualMetrics();

    // Filter only Formula metrics
    List<FormulaMetric> formulaMetrics = filterMetrics(manualMetrics);

    // Each formula metric
    for (FormulaMetric formulaMetric : formulaMetrics) {
      String formula = formulaMetric.getFormula();
      String replacedFormula = String.valueOf(formula);

      // Each word of the formula
      List<String> words = FormulaUtil.extractWords(formula);

      for (String word : words) {
        // Checks for metric
        Metric wordMetric = metricFinder.findByKey(word);

        // Metric exists and is numeric
        if (wordMetric != null && !wordMetric.isDataType()) {
          // Obtain measure
          Measure wordMeasure = (Measure) context.getMeasure(wordMetric.getKey());

          // Measure exists and has value
          if (wordMeasure != null && MeasureUtils.hasValue(wordMeasure)) {
            // Obtain value
            Double wordValue = wordMeasure.getValue();

            // Replace value in expression
            replacedFormula = replacedFormula.replaceFirst(word, wordValue.toString());
          }
        }
      }

      // Execute formula
      saveFormulaValue(context, formulaMetric.getMetric(), replacedFormula);
    }
  }
  
  /**
   * Executes formula and saves measure
   * 
   * @param context
   *          Decorator context
   * @param metric
   *          Metric to save
   * @param replacedFormula
   *          Formula to execute
   */
  private void saveFormulaValue(MeasureComputerContext context, Metric metric, String replacedFormula) {
    if (replacedFormula != null) {
      Double result = FormulaUtil.executeFormula(replacedFormula);

      if (result != null) {
        // Infinity value
        if (result.equals(Double.POSITIVE_INFINITY) || result.equals(Double.NEGATIVE_INFINITY)) {
          LOG.error("Invalid operation: division by zero");
        }
        // NaN value
        else if (result.equals(Double.NaN)) {
          LOG.error("Invalid operation: indeterminacy");
        }
        // Good value
        else {
          // Save measure
          context.addMeasure(metric.getKey(), result);
        }
      }
    }
  }
  
  /**
   * Get all available metrics in sonarqube instance through metric finder
   * @return an array of string with metric keys
   */
  public String[] getAllMetricsKeys() {
    List<Metric> allMetrics = new ArrayList<Metric>(metricFinder.findAll());
    
    ArrayList<String> allMetricKeys = new ArrayList<String>();
    for(Metric metric : allMetrics) {
      allMetricKeys.add(metric.getKey());
    }
       
    return allMetricKeys.toArray(new String[]{});
  }
  
  /**
   * Get all core metrics keys
   * 
   * @return a list of metric keys for all core metrics
   */
  public String[] getAllCoreMetricsKeys() {
    List<String> metricKeys = new ArrayList<String>();
    List<Metric> metrics = CoreMetrics.getMetrics();
    for(Metric metric : metrics) {
      metricKeys.add(metric.getKey());
    }
    return metricKeys.toArray(new String[]{});
  }
  
  /**
   * Get all manual metrics that are "formula" based
   * 
   * @return the manual metrics keys
   */
  private String[] getFormulaMetrics() {
    LOG.debug("Getting formula metrics from manual metrics...");
    
    // get tManual metrics
    List<Metric> manualMetrics = getManualMetrics();

    // Filter only Formula metrics
    List<FormulaMetric> formulaMetrics = filterMetrics(manualMetrics);
    
    ArrayList<String> formulaMetricKeys = new ArrayList<String>();
    for(FormulaMetric metric : formulaMetrics) {
      formulaMetricKeys.add(metric.getMetric().getKey());
      LOG.debug("Formula Metric: "+metric.getMetric().getKey());
    }
       
    return formulaMetricKeys.toArray(new String[]{});
    
  }
  
  /**
   * Obtains manual metrics defined by the SonarQube user
   * 
   * @return the list of manual metrics
   */
  protected final List<Metric> getManualMetrics() {
    // All SonarQube metrics
    List<Metric> allMetrics = new ArrayList<Metric>(metricFinder.findAll());

    // Manual metrics
    List<Metric> manualMetrics = new ArrayList<Metric>();

    for (Metric metric : allMetrics) {
      // if it's a manual metric
      if (metric != null && metric.getUserManaged()) {
        LOG.debug("Manual metric: "+metric.getName() + " - Description: "+metric.getDescription());
        manualMetrics.add(metric);
      }
    }

    return manualMetrics;
  }
  
  /**
   * Filter the given metrics by FORMULA description
   * 
   * @param mMetrics
   *          manual metrics
   * @return a list of formula metrics
   */
  public final List<FormulaMetric> filterMetrics(List<Metric> mMetrics) {
    List<FormulaMetric> formulaMetrics = new ArrayList<FormulaMetric>();

    // For each manual metric
    for (Metric metric : mMetrics) {
      // DATA metrics are not allowed
      if ( !metric.isDataType()) {
        // Detect formula
        String description = metric.getDescription();
        String formula = extractFormula(description);

        if (StringUtils.isNotBlank(formula)) {
          // Create formula
          FormulaMetric formulaMetric = new FormulaMetric(metric, formula);
          LOG.debug("Formula metric found: "+metric.getKey()+" = "+ formula);
          formulaMetrics.add(formulaMetric);
        }
      }
    }

    return formulaMetrics;
  }
  
  /**
   * @param metricDescription the metric description
   * @return the formula extracted of the metric description or null if there is no formula
   */
  protected final String extractFormula(String metricDescription) {
    String formula = null;

    if (StringUtils.isNotBlank(metricDescription)) {
      // Check definition
      boolean startCondition = metricDescription.startsWith(FORMULA_PREFIX);
      boolean endCondition = metricDescription.endsWith(FORMULA_SUFFIX);

      if (startCondition && endCondition) {
        // Remove prefix and suffix
        String withoutPrefix = metricDescription.replaceFirst(Pattern.quote(FORMULA_PREFIX), "");
        formula = withoutPrefix.substring(0, withoutPrefix.length() - 1);
      }
    }

    return formula;
  }


}