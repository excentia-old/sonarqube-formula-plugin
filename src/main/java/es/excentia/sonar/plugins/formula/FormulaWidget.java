/*
 * Sonar Formula Plugin
 * Copyright (C) 2012 excentia
 * contact@excentia.es
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02
 */
package es.excentia.sonar.plugins.formula;

import org.sonar.api.web.AbstractRubyTemplate;
import org.sonar.api.web.Description;
import org.sonar.api.web.RubyRailsWidget;
import org.sonar.api.web.UserRole;
import org.sonar.api.web.WidgetCategory;
import org.sonar.api.web.WidgetProperties;
import org.sonar.api.web.WidgetProperty;
import org.sonar.api.web.WidgetPropertyType;

/**
 * Formula Plugin Widget
 */
@UserRole(UserRole.USER)
@Description("Shows Formula Plugin widget")
@WidgetCategory("Formula")
@WidgetProperties({
  @WidgetProperty(key = "suffix", description = "Text that will be shown behind the formula result", type = WidgetPropertyType.STRING),
  @WidgetProperty(key = "title", description = "Widget title", defaultValue = "My metric", type = WidgetPropertyType.STRING),
  @WidgetProperty(
      key = "formulaExp",
      description = "Ruby expression for calculating the new metric. Terms of the expression must be separated by blanks. Only numeric metrics are allowed. Ex: (lines - comment_lines - ncloc)",
      defaultValue = "ncloc", type = WidgetPropertyType.STRING) })
public class FormulaWidget extends AbstractRubyTemplate implements RubyRailsWidget {

  /**
   * Returns Widget ID
   */
  public final String getId() {
    return "formulawidget";
  }

  /**
   * Returns Widget Title
   */
  public final String getTitle() {
    return "Formula Widget";
  }

  /**
   * Returns Template Path
   */
  protected final String getTemplatePath() {
    return "/FormulaWidget.html.erb";
  }
}