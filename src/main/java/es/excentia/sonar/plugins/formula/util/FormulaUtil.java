/*
 * Sonar Formula Plugin
 * Copyright (C) 2012 excentia
 * contact@excentia.es
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02
 */
package es.excentia.sonar.plugins.formula.util;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import org.sonar.api.utils.log.Loggers;

import bsh.EvalError;
import bsh.Interpreter;

/**
 * Formula Utility Class
 */
public final class FormulaUtil {

  private static final org.sonar.api.utils.log.Logger LOG = Loggers.get(FormulaUtil.class);

  /**
   * Private constructor (Singleton pattern)
   */
  private FormulaUtil() {
  }

  /**
   * Extracts each word of the phrase
   * 
   * @param phrase
   *          Formula expression
   * @return words list of the phrase
   */
  public static List<String> extractWords(String phrase) {
    
    LOG.debug("Extracting words from formula...");
    List<String> words = new ArrayList<String>();

    if (phrase != null) {
      try {
        // Regular expression
        Pattern pattern = Pattern.compile("[a-zA-Z][a-zA-Z-_0-9]*");

        // Extract words
        Matcher matcher = pattern.matcher(phrase);

        // Store words
        while (matcher.find()) {
          LOG.debug("word found: "+matcher.group());
          words.add(matcher.group());
        }

      } catch (PatternSyntaxException exception) {
        LOG.error("Pattern not valid: " + exception.getMessage(), exception);
      }
    }

    return words;
  }

  /**
   * Executes the formula
   * 
   * @param formula
   *          formula expression in JavaScript notation
   * @return result of the expression, null if expression is not correct
   */
  public static Double executeFormula(String formula) {
    Interpreter interpreter = new Interpreter();
    Double value = null;

    try {
      value = (Double) interpreter.eval(formula);
    } catch (EvalError exception) {
      LOG.debug("Expression is not correct: " + exception.getMessage(), exception);
    }

    return value;
  }
}