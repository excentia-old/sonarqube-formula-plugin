/*
 * Sonar Formula Plugin
 * Copyright (C) 2012 excentia
 * contact@excentia.es
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02
 */
package es.excentia.sonar.plugins.formula;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class FormulaManualMeasuresWidgetTest {

  private final FormulaManualMeasuresWidget widget = new FormulaManualMeasuresWidget();

  @Test
  public void testWidgetsId() {
    assertEquals("Widget ID must be formulamanualmeasureswidget", widget.getId(), "formulamanualmeasureswidget");
  }

  @Test
  public void testWidgetsTitle() {
    assertEquals("Widget title must be Formula manual measures Widget", widget.getTitle(), "Formula manual measures Widget");
  }

  @Test
  public void testWidgetsPath() {
    assertEquals("Widget template path must be /FormulaManualMeasuresWidget.html.erb", widget.getTemplatePath(),
        "/FormulaManualMeasuresWidget.html.erb");
  }
}