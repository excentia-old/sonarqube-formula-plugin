/*
 * Sonar Formula Plugin
 * Copyright (C) 2012 excentia
 * contact@excentia.es
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02
 */
package es.excentia.sonar.plugins.formula;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.sonar.api.ce.measure.Component;
import org.sonar.api.ce.measure.MeasureComputer.MeasureComputerDefinition;
import org.sonar.api.ce.measure.test.TestComponent;
import org.sonar.api.ce.measure.test.TestComponent.FileAttributesImpl;
import org.sonar.api.ce.measure.test.TestMeasureComputerContext;
import org.sonar.api.ce.measure.test.TestMeasureComputerDefinitionContext;
import org.sonar.api.ce.measure.test.TestSettings;
import org.sonar.api.measures.CoreMetrics;
import org.sonar.api.measures.Metric;
import org.sonar.api.measures.Metric.ValueType;
import es.excentia.sonar.plugins.formula.model.FormulaMetric;

import org.sonar.api.measures.MetricFinder;

/**
 * Since SQ 5.3 the decorator becomes a measure computer. 
 * Plugin will not work because of the new architecture. We have disabled the tests that are failing
 * and the extension is removed from the plugin. However, we will keep the code... maybe in the 
 * future the problem is fixed. 
 * 
 * @author acalero
 *
 */
public class FormulaMeasureComputerTest {
  
  private FormulaMeasureComputer formulaMeasureComputer;
  private MeasureComputerDefinition measureComputerDefinition;
  
  private Metric outputMetric;
  private Metric nclocMetric;
  private Metric classesMetric;
  
  private MetricFinder metricFinder;
  private ArrayList<Metric> allMetrics;
  
  @Before
  public void setUp() {
    
    // Define input metrics to use
    nclocMetric = Mockito.mock(Metric.class);
    classesMetric = Mockito.mock(Metric.class);
    
    // Define output metrics to use
    outputMetric = Mockito.mock(Metric.class);

    Mockito.when(outputMetric.getUserManaged()).thenReturn(true);
    Mockito.when(outputMetric.getDomain()).thenReturn("Formula");
    Mockito.when(outputMetric.getKey()).thenReturn("key");
    Mockito.when(outputMetric.getName()).thenReturn("name");
    Mockito.when(outputMetric.getDescription()).thenReturn("Formula[ncloc / classes]");
    Mockito.when(outputMetric.getType()).thenReturn(ValueType.FLOAT);
    
    // Metric finder to lookup metrics
    metricFinder = mock(MetricFinder.class);
    
    // all metrics
    allMetrics = new ArrayList<Metric>();
    allMetrics.add(CoreMetrics.NCLOC);
    allMetrics.add(CoreMetrics.CLASSES);
    allMetrics.add(outputMetric);
    Mockito.when(metricFinder.findAll()).thenReturn(allMetrics);
    
    // by key
    Mockito.when(metricFinder.findByKey(CoreMetrics.NCLOC_KEY)).thenReturn(nclocMetric);
    Mockito.when(metricFinder.findByKey(CoreMetrics.CLASSES_KEY)).thenReturn(classesMetric);
    Mockito.when(metricFinder.findByKey(outputMetric.getKey())).thenReturn(outputMetric);
    
    // build measure computer and context definition
    formulaMeasureComputer = new FormulaMeasureComputer(metricFinder);
    TestMeasureComputerDefinitionContext definitionContext = new TestMeasureComputerDefinitionContext();
    
    // define measure computer (input and output metrics)
    measureComputerDefinition = formulaMeasureComputer.define(definitionContext);
  }
  
  public void test_definition() {
    assertNotNull(measureComputerDefinition);
    assertTrue(measureComputerDefinition.getInputMetrics().contains(CoreMetrics.NCLOC_KEY));
    assertTrue(measureComputerDefinition.getInputMetrics().contains(CoreMetrics.CLASSES_KEY));
    assertTrue(measureComputerDefinition.getInputMetrics().contains(outputMetric.getKey()));
    assertTrue(measureComputerDefinition.getOutputMetrics().contains(outputMetric.getKey()));
  }
  
  @Test
  public void get_all_metrics() {
    assertEquals("All metrics", formulaMeasureComputer.getAllMetricsKeys(), Arrays.asList(CoreMetrics.NCLOC_KEY, CoreMetrics.CLASSES_KEY, outputMetric.getKey()).toArray());
  }
  
  @Test
  public void get_manual_metrics() {
    assertEquals("Manual metrics", formulaMeasureComputer.getManualMetrics(), Arrays.asList(outputMetric));
  }
  
  @Test
  public void extract_null_value_formula() {
    String description = null;
    String description2 = "";

    assertNull("Null formula", formulaMeasureComputer.extractFormula(description));
    assertNull("Null formula", formulaMeasureComputer.extractFormula(description2));
  }

  @Test
  public void extract_wrong_formula() {
    String description = "lines - ncloc";
    String description2 = "Formula[lines - ncloc";
    String description3 = "lines - ncloc]";

    assertNull("Null formula", formulaMeasureComputer.extractFormula(description));
    assertNull("Null formula", formulaMeasureComputer.extractFormula(description2));
    assertNull("Null formula", formulaMeasureComputer.extractFormula(description3));
  }

  @Test
  public void extract_correct_formula() {
    String description = "Formula[lines - ncloc]";

    assertEquals("Extracted formula", formulaMeasureComputer.extractFormula(description), "lines - ncloc");
  }

  @Test
  public void filter_manual_metrics_and_find_formulas() {
    Metric metric1 = Mockito.mock(Metric.class);
    Metric metric2 = Mockito.mock(Metric.class);
    Metric metric3 = Mockito.mock(Metric.class);
    Metric metric4 = Mockito.mock(Metric.class);

    Mockito.when(metric1.isDataType()).thenReturn(false);
    Mockito.when(metric2.isDataType()).thenReturn(false);
    Mockito.when(metric3.isDataType()).thenReturn(true);
    Mockito.when(metric4.isDataType()).thenReturn(false);

    Mockito.when(metric1.getDescription()).thenReturn("Formula[ncloc * 10]");
    Mockito.when(metric2.getDescription()).thenReturn("Management");
    Mockito.when(metric3.getDescription()).thenReturn("Formula[commits_by_developer]");
    Mockito.when(metric4.getDescription()).thenReturn("Formula[lines - ncloc]");

    List<Metric> manualMetrics = Arrays.asList(metric1, metric2, metric3, metric4);

    List<FormulaMetric> formulaMetrics = formulaMeasureComputer.filterMetrics(manualMetrics);

    assertSame("Two formula metrics detected", formulaMetrics.size(), 2);
  }

  public void compute_measure_for_project() {
   
    TestComponent component = new TestComponent("project", Component.Type.PROJECT, null);
    TestSettings contextSettings = new TestSettings();
    TestMeasureComputerDefinitionContext definitionContext = new TestMeasureComputerDefinitionContext();
    
    // define measure computer (input and output metrics)
    measureComputerDefinition = formulaMeasureComputer.define(definitionContext);
    
    TestMeasureComputerContext context = new TestMeasureComputerContext(component, contextSettings, measureComputerDefinition);
    context.addInputMeasure("ncloc", 100);
    context.addInputMeasure("classes", 20);
    
    formulaMeasureComputer.compute(context);

    assertEquals("Formula value", context.getMeasure(outputMetric.getKey()).getDoubleValue(), 5.0);
  }
  
  public void compute_measure_for_file() {
   
    TestComponent component = new TestComponent("file", Component.Type.FILE, new FileAttributesImpl("java", true));
    TestSettings contextSettings = new TestSettings();
    
    TestMeasureComputerContext context = new TestMeasureComputerContext(component, contextSettings, measureComputerDefinition);
    context.addInputMeasure("ncloc", 100);
    context.addInputMeasure("classes", 20);

    formulaMeasureComputer.compute(context);

    assertEquals("Formula value", context.getMeasure(outputMetric.getKey()).getDoubleValue(), 5.0);
  }
  
  public void compute_measure_for_directory() {
   
    TestComponent component = new TestComponent("directory", Component.Type.DIRECTORY, null);
    TestSettings contextSettings = new TestSettings();
    
    TestMeasureComputerContext context = new TestMeasureComputerContext(component, contextSettings, measureComputerDefinition);
    context.addInputMeasure("ncloc", 100);
    context.addInputMeasure("classes", 20);
    
    formulaMeasureComputer.compute(context);

    assertEquals("Formula value", context.getMeasure(outputMetric.getKey()).getDoubleValue(), 5.0);
  }
  
  public void compute_measure_for_module() {
   
    TestComponent component = new TestComponent("module", Component.Type.MODULE, null);
    TestSettings contextSettings = new TestSettings();
    
    TestMeasureComputerContext context = new TestMeasureComputerContext(component, contextSettings, measureComputerDefinition);
    context.addInputMeasure("ncloc", 100);
    context.addInputMeasure("classes", 20);
    
    formulaMeasureComputer.compute(context);

    assertEquals("Formula value", context.getMeasure(outputMetric.getKey()).getDoubleValue(), 5.0);
  }
  
}