/*
 * Sonar Formula Plugin
 * Copyright (C) 2012 excentia
 * contact@excentia.es
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02
 */
package es.excentia.sonar.plugins.formula;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class FormulaWidgetTest {

  private final FormulaWidget widget = new FormulaWidget();

  @Test
  public void testWidgetsId() {
    assertEquals("Widget ID must be formulawidget", widget.getId(), "formulawidget");
  }

  @Test
  public void testWidgetsTitle() {
    assertEquals("Widget title must be Formula Widget", widget.getTitle(), "Formula Widget");
  }

  @Test
  public void testWidgetsPath() {
    assertEquals("Widget template path must be /FormulaWidget.html.erb", widget.getTemplatePath(), "/FormulaWidget.html.erb");
  }
}