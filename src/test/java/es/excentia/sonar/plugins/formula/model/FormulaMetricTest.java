/*
 * Sonar Formula Plugin
 * Copyright (C) 2012 excentia
 * contact@excentia.es
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02
 */
package es.excentia.sonar.plugins.formula.model;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.mockito.Mockito;
import org.sonar.api.measures.Metric;

public class FormulaMetricTest {

  @Test
  public void testCreation() {
    Metric metric = Mockito.mock(Metric.class);
    FormulaMetric formulaMetric = new FormulaMetric(metric, "lines - ncloc");

    assertEquals("Formula metric", formulaMetric.getMetric(), metric);
    assertEquals("Formula expression", formulaMetric.getFormula(), "lines - ncloc");
  }
}