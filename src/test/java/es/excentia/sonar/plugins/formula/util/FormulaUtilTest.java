/*
 * Sonar Formula Plugin
 * Copyright (C) 2012 excentia
 * contact@excentia.es
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02
 */
package es.excentia.sonar.plugins.formula.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.List;

import org.junit.Test;

public class FormulaUtilTest {

  @Test
  public void testInstance() throws SecurityException, NoSuchMethodException, IllegalArgumentException, InstantiationException,
    IllegalAccessException, InvocationTargetException {
    Constructor<FormulaUtil> constructor = FormulaUtil.class.getDeclaredConstructor();
    constructor.setAccessible(true);

    assertNotNull("Testing constructor", constructor.newInstance());
  }

  @Test
  public void testExtractFormula() {
    String formula = "(ncloc / classes)";

    List<String> words = FormulaUtil.extractWords(formula);

    assertSame(words.size(), 2);
    assertTrue(words.contains("ncloc"));
    assertTrue(words.contains("classes"));

    formula = "(duplicated_lines + dead-code + potential-dead-code) / ncloc";

    words = FormulaUtil.extractWords(formula);

    assertSame(words.size(), 4);
    assertTrue(words.contains("duplicated_lines"));
    assertTrue(words.contains("dead-code"));
    assertTrue(words.contains("potential-dead-code"));
    assertTrue(words.contains("ncloc"));
  }

  @Test
  public void testExecuteFormula() {
    String formula = "";
    assertNull(FormulaUtil.executeFormula(formula));

    formula = "(100.0 + 20.0) / 5.0";
    assertEquals(FormulaUtil.executeFormula(formula), Double.valueOf(24.0));

    formula = "Math.pow(2.0, 5.0)";
    assertEquals(FormulaUtil.executeFormula(formula), Double.valueOf(32.0));

    formula = "10.0 / 0.0";
    assertEquals(FormulaUtil.executeFormula(formula), Double.valueOf(Double.POSITIVE_INFINITY));

    formula = "0.0 / 0.0";
    assertEquals(FormulaUtil.executeFormula(formula), Double.valueOf(Double.NaN));

    formula = "enecloc * 2.0";
    assertNull(FormulaUtil.executeFormula(formula));
  }
}